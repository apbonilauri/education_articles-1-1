# Immersion in learning environments: Situational measurement and multi-dimensional Computerized Adaptive Testing using a reproducible research methodology

Joao Vissoci
Elias Carvalho
Adelia Batilana
Ricardo Pietrobon

<!-- Joao, reproducible here means that we will insert the R code inside this document --> 
<!-- MIRT, MAT -->


## Abstract
<!-- add at the end --> 

## Introduction
Immersion is a multifaceted construct, with hardly any two specialists agreeing on what exactly it means. <!-- add three main papers -->  

Immersion has been defined as <!-- here we should reuse the text from Xavier immersion, but also applying a diff or some other sort of comparison to ensure that we are not self-plagiarizing --> 
Given the multi-dimensional and ever changing nature of the concept of immersion, the idea of multidimensional adaptive assessment seems like a natural fit. Briefly, Multidimensional Computerized Adaptive Testing (MCAT) <!-- add definition here from the MCAT book --> 

## Methods

<!-- how can we automatically import from the xavier immersion paper?? can we save stuff as gists and then apply regexp to extract only the parts we want?? --> 

## Results
<!-- how can we migrate the sessions from the methods into here to enhance reliability? --> 

## Discussion
To the best of our knowledge, this the first article describing a Multidimensional Computerized Adaptive Test applied to immersive learning. Our results have demonstrated that <!-- add four main results --> 

### Result 1
### Result 2
### Result 3
### Result 4

### Limitations
<!-- some portions of Xavier immersion apply, find way to reuse --> 

### Future
<!-- some portions of Xavier immersion apply, find way to reuse --> 



## References
<!-- Elias, please google markdown and bibliographic references to see whether we can find a way to make this easier --> 