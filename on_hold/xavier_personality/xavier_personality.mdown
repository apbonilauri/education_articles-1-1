# Situational personality measurement using Multidimensional Computerized Adaptive Testing for immersive education: A reproducible research project
<!-- OI PESSOAL, ME DESCULPEM, ESTOU APENAS BRINCANDO COM O GIT. -->
<!-- sjt http://goo.gl/wIwnc -->
<!-- shiny app 
http://www.econometricsbysimulation.com/2013/05/item-analysis-app-shiny-code.html -->

## Abstract
<!-- will write at the end --> 

## Introduction
While executive coaching methods strive to provide feedback to novice employees that is highly tailored to their needs, online education is still using the "one size fits all" model. While this model is likely less effective in improving individual performance, current technologies now allow for course content to be delivered in a context that aligns with behavioral characteristics of the trainee. Ideally, however, these courses should adapt not only to specific situations but also to the personality characteristics of the trainee. Such interaction between personality and situation measured in an adaptive manner is, to the best of our knowledge, currently not available.
<!-- cite articulate, knewton, and preciseskills -->


<!-- rprior review kindle personality -->

<!-- Joao, please add some key references to the situational personality literature here along with the main points you would like to make -->

When it comes to the adaptive measurement of personality, the literature to date can be roughly categorized into the unidimensional and multidimensional branches. 

<!-- Joao, please add here the key references on both placebo on zotero, the statements you would like to make about each -->

Given this gap in the literature, the objective of this article is to present reproducible results in the creation of a situated,item bank calibrated using multidimensional item response theory and deployed using multidimensional computerized adaptive tests. 

## Methods

## Construct definition

We selected the domains of intro/extroversion as well as ins/stability as the two axes of our item banks. This choice is justified since

<!-- Joao, please add references in zotero along with full text pdf as well as sentences describing your choice -->

<!-- Joao, please add the specific definitions you would like to give to each of the constructs along with the references used to back them up -->

## Item writing
Items were written by three researchers (RP, JRV, and TR) following the definitions provided in the previous section. A group of 
<!-- number -->
of items was initially written for each construct based on face validity, as this item sample size has been previously prescribed as a minimum to ensure bank integrity.
<!-- Joao, do you remember the reference for this? I think it came from one of the kindle books on CAT, but I am not sure -->


### Situations

Although individual items were designed to align with the two main axes of our scale, in order to achieve a situational 
<!-- situation: familia amorosa, backstabbing workplace --> 
<!-- emotion calm vs. stress --> 
<!-- # Xavier intro/extroversion

continuei pensando na ideia do joao de AIG e quanto mais eu penso mais eu gosto do conceito. so que estava pensando se nos nao deveriamos usar dois "situational constructs":
A. home/work - esse daqui vai potencialmente injetar DIF an intro/extroversion
B. high/low stress environments - DIF potencialmente injetado na in/stability
cada um teria 10 situacoes especificas que seriam injetadas aleatoriamente. ou seja, home ou work situation randomizada pra anexar a um item de intro/extroversion e a mesma coisa pro outro

o problema que eu vejo com esse design, e ele na verdade se aplica ate a ideia mais simples de ontem e que digamos que a gente liste la 5 coisas que sao de home situation e 5 de work environment. mas dai says who? ou seja, quem garante que o que a gente acha que e casa na cabeca da pessoa nao e trabalho? nao sei como validar isso

o que voces acham? assim que receber a resposta eu vou comecar a escrever itens pra tudo isso
 -->


### Graphical exploratory analysis

[qgraph](http://goo.gl/gZisY)


### Full information item response factor analysis

<!-- available within the mirt package, function mirt -->

<!-- Muraki E, Carlson EB (1995). \Full-Information Factor Analysis for Polytomous Item Responses." Applied Psychological Measurement, 19, 73-90. -->




### Multidimensional item response modeling
4PL

<!-- Wirth RJ, Edwards MC (2007). \Item Factor Analysis: Current Approaches and Future Directions." Psychological Methods, 12(1), 58-79 -->

[mirt](http://www.jstatsoft.org/v48/i06/paper)

### Situational differential item and step functioning

[Penfield](http://www.education.miami.edu/psell/Documents/Publications/_9_%20Penfield%20et%20al..pdf)


### Multidimensional Computerized Adaptive Testing

[MAT](http://cran.r-project.org/web/packages/MAT/MAT.pdf)


### Reproducible research protocol

As partially described in previous sections, we followed a reproducible research protocol along the lines previously described by our group [(Vissoci, 2013)](). Briefly, all data sets, scripts, templates, software and workflows generated under this project were deposited under the public repositories [Github]() and [figshare](). For access to the specific data for this project please refer to 
<!-- please add reference once available -->

## Results

## Discussion

### Primacy and summary

### Result 1
### Result 2
### Result 3
### Result 4
### Limitations

### Future

## References

Brown, R. D., & Harvey, R. J. (1998, April). Computer-adaptive testing and test-retest reliability in a “Big-Five” personality inventory. Paper presented at the Annual Conference of the Society for Industrial and Organizational Psychology, Dallas.  

[Forbey, 2007](http://www.ncbi.nlm.nih.gov/pubmed/17371120)

[Hol, 2008](http://iacat.org/sites/default/files/biblio/Computerized%20Adaptive%20Testing%20of%20Personality%20Traits.pdf)

[Makrasnky, 2013](http://www.ncbi.nlm.nih.gov/pubmed/22357698)

[Schneider, 2009](http://publicdocs.iacat.org/cat2010/cat09schneider.pdf)

[Smid](http://www.picompany.nl/uploads/5061ac26a91178.76681947.pdf)

[Simms, 2005](http://wings.buffalo.edu/psychology/labs/simmslab/SNAPCAT.pdf)

[Winarto, 2009](http://hmi.ewi.utwente.nl/verslagen/capita-selecta/CS-Winarto-Marielle.pdf)

1: Simms LJ, Goldberg LR, Roberts JE, Watson D, Welte J, Rotterman JH. Computerized adaptive assessment of personality disorder: introducing the CAT-PD project. J Pers Assess. 2011 Jul;93(4):380-9. doi: 10.1080/00223891.2011.577475. PubMed PMID: 22804677; PubMed Central PMCID: PMC3400119.


2: Makransky G, Mortensen EL, Glas CA. Improving personality facet scores with multidimensional computer adaptive testing: an illustration with the NEO PI-R. Assessment. 2013 Feb;20(1):3-13. doi: 10.1177/1073191112437756. Epub 2012 Feb 21. PubMed PMID: 22357698.


3: Greene RL. 2010 Bruno Klopfer Distinguished Contribution Award. Some considerations for enhancing psychological assessment. J Pers Assess. 2011 May;93(3):198-203. doi: 10.1080/00223891.2011.558879. PubMed PMID: 21516578.


4: DeWitt EM, Stucky BD, Thissen D, Irwin DE, Langer M, Varni JW, Lai JS, Yeatts KB, Dewalt DA. Construction of the eight-item patient-reported outcomes measurement information system pediatric physical function scales: built using
item response theory. J Clin Epidemiol. 2011 Jul;64(7):794-804. doi: 10.1016/j.jclinepi.2010.10.012. Epub 2011 Feb 2. PubMed PMID: 21292444; PubMed Central PMCID: PMC3100387.


5: Savage C, Amanali S, Andersson A, Löhr SC, Eliasson Z, Eriksson H, Erlandsson A, Goobar S, Holm J, Johansson C, Langendahl E, Lindberg A, Lundin J, Uhrdin A, Schwarz Uv. Turning the tables: when the student teaches the professional -- a case description of an innovative teaching approach as told by the students. Nurse Educ Today. 2011 Nov;31(8):803-8. doi: 10.1016/j.nedt.2010.11.023. Epub 2010 Dec 16. PubMed PMID: 21167626.


6: Van der Molen MJ, Van Luit JE, Van der Molen MW, Klugkist I, Jongmans MJ.
Effectiveness of a computerised working memory training in adolescents with mild 
to borderline intellectual disabilities. J Intellect Disabil Res. 2010
May;54(5):433-47. doi: 10.1111/j.1365-2788.2010.01285.x. PubMed PMID: 20537049.


7: Lahmann C, Jacobs F, Pieh C, Henningsen P, Loew T. [Development of a novel
device for adaptive testing according to ICD-10: PsyPAM--psychosomatic
physician-patient module]. Psychother Psychosom Med Psychol. 2010
May;60(5):180-4. doi: 10.1055/s-0029-1241828. Epub 2009 Nov 11. German. PubMed
PMID: 19908174.


8: Forkmann T, Boecker M, Norra C, Eberle N, Kircher T, Schauerte P, Mischke K,
Westhofen M, Gauggel S, Wirtz M. Development of an item bank for the assessment
of depression in persons with mental illnesses and physical diseases using Rasch 
analysis. Rehabil Psychol. 2009 May;54(2):186-97. doi: 10.1037/a0015612. PubMed
PMID: 19469609.


9: Barrada JR, Olea J, Abad FJ. Rotating item banks versus restriction of maximum
exposure rates in computerized adaptive testing. Span J Psychol. 2008
Nov;11(2):618-25. PubMed PMID: 18988447.


10: Becker J, Fliege H, Kocalevent RD, Bjorner JB, Rose M, Walter OB, Klapp BF.
Functioning and validity of a Computerized Adaptive Test to measure anxiety
(A-CAT). Depress Anxiety. 2008;25(12):E182-94. doi: 10.1002/da.20482. PubMed
PMID: 18979458.


11: Cheng Y, Chang HH. The maximum priority index method for severely constrained
item selection in computerized adaptive testing. Br J Math Stat Psychol. 2009
May;62(Pt 2):369-83. doi: 10.1348/000711008X304376. Epub 2008 Jun 2. PubMed PMID:
18534047.


12: Harniss M, Amtmann D, Cook D, Johnson K. Considerations for developing
interfaces for collecting patient-reported outcomes that allow the inclusion of
individuals with disabilities. Med Care. 2007 May;45(5 Suppl 1):S48-54. PubMed
PMID: 17443119; PubMed Central PMCID: PMC2822706.


13: Forbey JD, Ben-Porath YS. Computerized adaptive personality testing: a review
and illustration with the MMPI-2 Computerized Adaptive Version. Psychol Assess.
2007 Mar;19(1):14-24. PubMed PMID: 17371120.


14: Petersen MA, Groenvold M, Aaronson N, Fayers P, Sprangers M, Bjorner JB;
European Organisation for Research and Treatment of Cancer Quality of Life Group.
Multidimensional computerized adaptive testing of the EORTC QLQ-C30: basic
developments and evaluations. Qual Life Res. 2006 Apr;15(3):315-29. PubMed PMID: 
16547770.


15: Wiener A, Marcus E, Mizrahi J. Objective measurement of knee extension force 
based on computer adaptive testing. J Electromyogr Kinesiol. 2007 Feb;17(1):41-8.
Epub 2006 Feb 23. PubMed PMID: 16497516.


16: Fries JF, Bruce B, Cella D. The promise of PROMIS: using item response theory
to improve assessment of patient-reported outcomes. Clin Exp Rheumatol. 2005
Sep-Oct;23(5 Suppl 39):S53-7. PubMed PMID: 16273785.


17: Gardner W, Shear K, Kelleher KJ, Pajer KA, Mammen O, Buysse D, Frank E.
Computerized adaptive measurement of depression: a simulation study. BMC
Psychiatry. 2004 May 6;4:13. PubMed PMID: 15132755; PubMed Central PMCID:
PMC416483.


18: Butcher JN, Perry J, Hahn J. Computers in clinical assessment: historical
developments, present status, and future challenges. J Clin Psychol. 2004
Mar;60(3):331-45. Review. PubMed PMID: 14981795.


19: Haley SM, Coster WJ, Andres PL, Ludlow LH, Ni P, Bond TL, Sinclair SJ, Jette 
AM. Activity outcome measurement for postacute care. Med Care. 2004 Jan;42(1
Suppl):I49-61. PubMed PMID: 14707755.


20: Bungay KM. Methods to assess the humanistic outcomes of clinical pharmacy
services. Pharmacotherapy. 2000 Oct;20(10 Pt 2):253S-258S. PubMed PMID: 11034051.


21: Waller NG, Reise SP. Computerized adaptive personality assessment: an
illustration with the Absorption scale. J Pers Soc Psychol. 1989
Dec;57(6):1051-8. PubMed PMID: 2614658.

# Appendix 
<!-- Joao, please add items here --> 

## Introversion/Extroversion

I am a very reserved person
I really like to be alone
Knowing a few selected people is far better than knowing a lot of people
I am always reflecting about things on my own
I tend to keep things to myself
I abhor going to parties where I know I will have to interact with people
An ideal day for me is when I can spend time alone
I enjoy activities where I can be alone, thinking about my stuff
I tend not to tell others about facts of my life
I takes me a lot of time to open up to somebody
My life is all but an open book
The anticipation of social gatherings makes me suffer.
I keep finding excuses not to attend social events where I will have to interact with lots of people
I love to be alone
I need a lot of time for myself
Being alone sometimes, is essential for my well being
Social events make me exhausted
I feel "out of air" when I am surrounded by people all the time
I prefer to keep my feelings to myself
I don't like to express my ideas to other people
The thought of having to make a public presentation scares me
I create strong connections only with very select people
When talking to somebody, I constantly feel like I am running out of interesting topics to discuss
I am very cautious in relation to most aspects of my life
It is very hard for me to find true friends
I hate being the center of attention
I feel very confident when I am with a large group of people
I feel awkward when starting a conversation
I tend to keep thinking about a topic over and over again
I fell very uncomfortable being the center of all attentions
Getting negative feedback makes me feel blue
When I have to say something, I tend to rehearse it a lot before trying it on a group of people
I am more of a listener than a talker
Most people would call me quiet or calm
I prefer to share (my thoughts, feelings) with a special person or a few close friends to sharing them with a large crowd
I always think carefully before saying something
I am sensitive to interpersonal details that most people don't notice
I like to watch something in detail before deciding to join
I hate interrupting others
After I hear a lot of information, I like to take my time to interpret it carefully
Getting together with large groups of people absolutely drains my energy
I don't like to talk on the phone
I am perceived by others as (being) sociable (or by others as a sociable person)
I feel comfortable in groups and like working in them.
I know a lot of people
I feel very comfortable while interacting with other people
I look forward to gatherings and parties
I know a lot of people and am constantly making new friends
I love being in other people's company 
I love being around other people
I cannot stand being alone
I can barely wait for social events
An ideal day for me is when I am surrounded by people
I just feel at home when I have to interact with people
I am constantly reaching out to people I don't know so that I can get to know them
Going out with friends is one of the most exciting things in my life
I generally seek new and exciting experiences and sensations
I feel drained when I have to stay home
I like to be the leader of my group
When I need to jump into a project, I love to be the one planning the whole thing
In a group of friends, I am always the one talking
I always manage to persuade my friends to join in my adventures
I love to assume risks and work on several projects at once
Being in the presence of my family and friends is an essential part of being me
I love to actively take part in groups
I really enjoy making new friends
At parties, I am the first to reach and the last to leave
When I have free time, I really enjoy being with other people
I prefer working in teams rather than working alone
When I have a problem I like to discuss it with somebody I trust rather than trying to solve it on my own
If I need some insight on a given issue, I always turn to other people
I am always sharing my feelings and thoughts with other people
I talk a lot
I tend to share personal aspects of my life with others
I go from just knowing somebody to feeling at ease with them in no time
I like small talk
Having a busy social life is absolutely essential for me
Thinking out loud helps me think things through
Solving problems with the help of others is something I almost invariably do
I am anything but a private person
I am a very transparent person, people get to know me very quickly
I really like to use facts (anecdotes)from my life as examples during conversations
I feel really comfortable around people
I hate being by myself

I am perceived by others as sociable
I feel comfortable in groups and like working in them.
I know a lot of people
I feel very comfortable while interacting with other people
I look forward to gatherings and parties
I know a lot of people and am constantly making new friends
I love being in other people's company 
I love being around other people
I cannot stand being alone
I can barely wait for social events
An ideal day for me is when I am surrounded by people
I feel at home when I have to interact with people
I am constantly reaching out to people I don't know so that I can get to know them
Going out with friends is one of the most exciting things in my life
I generally seek out new and exciting experiences and sensations
I feel drained when I have to stay home
I like to be the leader of my group
When I need to jump into a project, I love to be the one planning the whole thing
In a group of friends, I am always the one talking
I always manage to persuade my friends into joining my adventures
I love to assume risks and work on several projects at once
Being in the presence of my family and friends is an essential part of being me
I love to actively take part in groups
I really enjoy making new friends
At parties, I am the first to come and the last to leave
When I have free time. I really enjoy staying (being) with other people
I prefer working in teams to working alone
When I have a problem, I like to discuss it with somebody I trust rather than try to solve it on my own
If I need some insight on a given issue, I always turn to other people
I am always sharing my feelings and thoughts with other people
I talk a lot
I tend to share personal aspects of my life with others
I go from knowing somebody to feeling at ease with them in no time
I like small talk
Having a busy social life is absolutely essential for me
Thinking out loud helps me think things through
Solving problems with the help of others is something I almost invariably do
I am anything but a private person
I am a very transparent person, people get to know me very quickly
I really like to use facts (anecdotes) from my life as examples during conversations
I feel really comfortable around people
I hate being by myself

## Stability/Instability

I can keep my cool when the pressure builds up
When I am in a delicate(sensitive, volatile) situation, I can usually see things from above (the outside)
I never lose my temper 
If I am unhappy, I can always pinpoint a specific reason. (for it)
If I am very happy, I can always pinpoint a specific reason.(for it)
When problems build up, I can remove myself from the situation and do an analysis from above (from the outside)
It is easy for me to dissect a problem to search for a solution (in search of a solution)
I can easily separate emotions from the solution of specific problems (to specific problems)
People very rarely get under my skin ( I rarely let people get under my skin)
I consider myself to have a very stable set of reactions to daily problems 
People usually come to me to for advice since I can always come up with a balanced opinion (since my opinions are always balanced)
I never make decisions on the spur of the moment
My humor is pretty stable; day in and day out
I am a very balanced person; no mood swings
High pressure situations rarely affect me
When a difficult situation arises, I can always look beyond the immediate problem to the ultimate solution
My colleagues seek my advice if they need a balanced opinion
If a situation requires attention, I can be very focused
When I need to focus, nothing distracts me
I very rarely have to go to somebody for advice on difficult situations (difficult issues), I can usually make decisions on my own
I feel in control of my life most of the time
I rarely feel overwhelmed; my life is under control
When I am set on a path, nothing will (make me deviate from it) deviate me
It is easy for me to make a decision and follow through with it
Even when a project demands a long period of time, I stay on track and keep working until completion
I am the kind of person that others go to for advice during periods of instability
I rarely feel irritated 
I rarely feel overly excited
No matter how complicated a situation might be, I am always confident that there is a solution
I tend to look at problems from a long-term perspective
I have a pretty well-defined set of principles that I use to make most of the decisions in my life
I like stable, predictable environments
Life is simple, it's just that some people tend to make it complicated
I have a few principles from which I derive most of my decisions
When I do one thing, I do that thing well ( When I do something, I do it well)
Some people find routines boring, I find routines a way to improve what I do, each time
Once I set my mind to something, I diligently pursue that objective until I accomplish it
I like to have simple and achievable objectives
I have a very constant way of behaving, what you see today is what you will see tomorrow ( My behaviour is very constant ; what you see today is what you see tomorrow)
It really takes a lot to get me out of balance
In my current job, on some days I feel outstanding and on others, I honestly feel like quitting
Problems frequently make me lose sleep at night
My day to day activities frequently lead to a lot of anxiety
Sometimes I just feel hopeless
I usually feel overwhelmed with my daily activities
When I am really anxious, I tend to talk to close friends to alleviate the tension
I am a nervous person, and that makes me uncomfortable
I am always losing sleep because of problems I can't seem to solve
Sometimes I feel like my life is a rollercoaster
If somebody catches me when I am in a bad mood, I can just explode
I can go from being extremely happy to very annoyed in no time
Some days I just feel tired and dont want to do anything
Certain people can annoy me to an extreme
Sometimes I feel very happy without a real reason
Sometimes I feel very sad without a real reason
Certain situations will make me lose my temper
Sometimes I  get distracted when I am supposed to be very focused
Being in front of large crowds make me nervous
Being in front of somebody who is my superior makes me nervous
Being in front of a large crowd makes me insecure
Being in front of somebody who is my superior makes me insecure
If I get stressed, ideas simply don't come to me
If the situation is stressful, I can simply freeze
In situations of stress, I can easily despair
Getting me out of my routine is not something I feel comfortable with
Anticipating a tough situation makes me anxious
Just the thought of being in front of a large crowd makes me anxious
Some days I  feel like I shouldn't have left my house
I can hardly predict what my humour will be like tomorrow
Sometimes I just feel like I am going to explode
Sometimes I feel like my life is completely out of control
I frequently feel overwhelmed
I constantly keep questioning myself on whether I am on the right path
After I make a decision I often go back and start evaluating it from different angles to see whether it was the right thing to do
I am frequently irritated
Sometimes I feel incredibly excited
If a situation is too complicated, I just feel hopeless
When a situation heats up, my first impulse is to find a way out
Life keeps changing on me, situations are never the same
Sometimes I feel very happy although I can't really explain why

