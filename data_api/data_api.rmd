# Enriching data from the comfort of your home: Combining schemaless databases, Linked Open Data and Open Access Data to enhance local data sets


<!-- https://github.com/ajdamico/usgsd -->

Marcelo Oliveira  
Darlan Chrystian  
Keila Santos  
Davi Prates  
Jose Eduardo Santana  
Jacson Barros  
Joao Vissoci  
Elias Carvalho  
Ricardo Pietrobon


<!-- 

foco api com mongodb por lod nao ser viavel pra dados com pacientes

 -->


## Abstract
<!-- will write at the end --> 

## Introduction
Not a month goes by without a new report being released with a new estimate in relation to the astounding data volume being generated around the world. As impressive as this volume might seem, when looked more closely this resource is highly disconnected, with data sets focusing on individual people hardly if ever being integrated to generate more encompassing information. 

<!-- LOD is successful, but does not address data sets with individual people. problems in biomedical and marketing research where matching is required. examples of matched data sets in biomedical and marketing research  -->

<!-- schemaless databases and APIs-->


The objective of this article is therefore to present an application to streamline the enrichment of data sets through the addition of a series socio-demographic, geolocation, and health-related variables.

## Methods

### [Informal use cases](http://www.agilemodeling.com/artifacts/systemUseCase.htm#FigureI1)

#### UC1 Name: Enrich variables using API
* Researcher calls dataRich function
* Researcher uploads local data set to R
* Researcher transforms variable names and alternative options to dataRich data standards <!-- dataRich standards are the standard variable names and alternative options used in the all datasets within our mongo repository -->
* Researcher establishes a connection between local R instance and public mongoDB API, which automatically updates the function in relation to any new database that might have been included under mongoDB
* dataRich function takes the following arguments
    * a vector with the matching variables between the local and remote dataset
    <!-- for the initial version let's only use categorical variables, at a later version we might add continuous variables -->
    * the unique id for the local observations
    * database in MongoDB where enriching variables will come from 
    <!-- this is the section that got updated by the time of the function got connected to the API -->
    * a vector with the variables to be enriched into the local dataset
 * query is sent over through the API and the matched observations in the remote database are selected. For example, if we are matching by a male, age 25-30, living in zip code 27710, then all individuals in the remote database matching those characteristics will be selected
 * if the enriching variable is a numeric variable, then the value brought over to the local database is the median 
 * if the enriching variable is a factor variable, then the value brought over to the local database is the mode

 <!--  -->



### Data sources

#### Open access data
three Brazilian datasets
#### Linked Open Data

### Schemaless database
MongoDB

### Data in need of enrichment
SUS

### Linked Open Data
how will convert RDF to bring into MongoDB

### API
security
JSON

### Access through statistical language
R packages


## Results

## Discussion

### Primacy and summary

### Result 1
### Result 2
### Result 3
### Result 4
### Limitations

### Future


<!-- bancos criando uma api http://goo.gl/KXpGJ e
dai servindo dados diretamente pra ferramenta de analise. no R e outras
linguagens de programacao usadas pra analise isso seria muito facil.

mas mesmo pras pessoas que usem software estatistico (sas, stata, spps,
etc) ao inves de linguagem estatistica/data mining (R, python, etc) o API
ainda seria vantajoso porque ele poderia alimentar uma app que dai sirva os
pesquisadores diretamente com csv ou sirva uma BI local ou sirva um
desenvolvedor que queira consumir os dados pra fazer qualquer coisa que der
na cabeca dessa pessoa. entao ficaria assim:

dados -> mongodb -> API;
API -> R;
API -> BI;
API -> "local app serving commonly used datasets";

a outra vantagem da API e que ela daria um nivel razoavel de security pra
gente.

um use case bem superficial entao seria:

1. researcher requests login/pwd for restricted data sets and accesses

public data sets through a public call using the API
2. educational program shows researchers how to access data and makes

available a small script library (in R this would be a package on github
accessible through devtools:install_github) teaching them how to query the
API.
3. researcher opens local data, pulls distant data in, enriches local

data through matching, and proceeds with scientific paper

nesse cenario o nosso papel seria converter os bancos de pesquisa pra um
formato ideal, o que necessariamente inclui meta-data e tambem pode querer
dizer que a gente comece publicando bancos aos pedacos pra evitar de so
soltar coisas gigantes. bancos de pesquisa sempre saem via password pra
pesquisadores especificos, esse seria o papel de governance. e a gente
tambem joga um monte de dado publico junto com a meta-data respectiva, aqui
isso ja ta pronto com o do SUS, se bem que tem outros - veja
http://goo.gl/TF1rp

sera que eu to viajando? so pra gente manter a coisa a nivel de python,
vejam http://goo.gl/CGd9U


taxonomy for enrichment (geolocation-related variables), data quality exploratory graphical analysis, connection mongodb, connection LOD, served through API (other paper), connection concerto

taxonomy is a separate paper

reproducible research - for data quality - full template with standard sections + tables + 

survey

criar um repositorio imenso de bancos acessados via API que possam ser usados pra enriquecer bancos locais -- isso vai ser util pra pesquisa biomedica e de marketing. ou seja, acho que finalmente achamos a tecnologia ideal pra data enrichment (que comecou com RDF na duke)
 -->
